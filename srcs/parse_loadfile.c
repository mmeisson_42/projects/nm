/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_loadfile.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 12:44:11 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/21 12:35:07 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "errors.h"
#include "libft.h"

void		parser_loadfile(struct s_file *file, struct s_ofile *ofile,
		struct mach_header *head, struct load_command *lcs)
{
	uint32_t			i;

	i = 0;
	while (i < head->ncmds)
	{
		if (file->file_size < ((uint64_t)lcs + 128) -
				(uint64_t)file->file_content)
			ft_ferror(file->file_name, CORRUPTED_ERROR);
		if (lcs->cmd == LC_SYMTAB)
			ofile->symtab = (struct symtab_command *)lcs;
		else if (lcs->cmd == LC_SEGMENT)
			ofile->section_number += ((struct segment_command *)lcs)->nsects;
		else if (lcs->cmd == LC_SEGMENT_64)
			ofile->section_number += ((struct segment_command_64 *)lcs)->nsects;
		lcs = (struct load_command *)((char *)lcs + lcs->cmdsize);
		i++;
	}
}
