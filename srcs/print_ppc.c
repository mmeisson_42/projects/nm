/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ppc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 14:34:04 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/13 14:49:04 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "ft_stdio.h"

static void		reverse(struct s_ofile *ofile, struct section *sect)
{
	if (ofile->reverse)
	{
		SWAP(sect->offset);
		SWAP(sect->size);
		SWAP(sect->addr);
	}
}

static void		print_text_32(struct s_ofile *ofile, struct section *sect)
{
	uint32_t		i;
	uint32_t		*tab;

	if (sect == NULL)
		return ;
	i = 0;
	reverse(ofile, sect);
	sect->size /= 4;
	tab = (uint32_t *)(ofile->file + sect->offset);
	while (i < sect->size)
	{
		if (i % 4 == 0)
		{
			ft_printf("%08llx\t", sect->addr);
			sect->addr += 16;
		}
		if (ofile->reverse)
			SWAP(tab[i]);
		ft_printf("%.8x ", tab[i]);
		i++;
		if (i % 4 == 0)
			ft_putchar('\n');
	}
	if (i % 4 != 0)
		ft_putchar('\n');
}

void			print_ppc(struct s_ofile *ofile)
{
	print_text_32(ofile, ofile->sections[ofile->nsect_text]);
}
