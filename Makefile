# *************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/28 14:28:00 by mmeisson          #+#    #+#              #
#    Updated: 2017/11/21 13:42:43 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			= ft_nm
OTOOL			= ft_otool

CC				= clang

CFLAGS			= -MD -Wall -Werror -Wextra

VPATH			= ./srcs/:

NM_SRCS			= nm.c errors.c get_file.c parse_header.c handle_symtab.c \
				  type.c parse_segment.c swap_bytes.c print_symbols.c \
				  arch_flags.c reverse_1.c reverse_2.c get_ofiles.c \
				  curr_arch_in_fat.c sort_functions.c get_current_arch.c \
				  \
				  parse_header.c parse_load_sections.c parse_loadfile.c \
				  parse_segment.c parse_symtab.c free_ofile.c check_fat_size.c \
				  norme.c

OTOOL_SRCS		= otool.c get_file.c errors.c get_current_arch.c \
				  handle_symtab.c get_ofiles.c swap_bytes.c parse_header.c \
				  reverse_1.c reverse_2.c parse_symtab.c sort_functions.c \
				  arch_flags.c parse_loadfile.c parse_load_sections.c \
				  curr_arch_in_fat.c parse_segment.c print_sections.c \
				  free_ofile.c print_ppc.c check_fat_size.c norme.c

INCS_PATHS		= ./incs/ ./libft/incs/ ./printf/incs/
INCS			= $(addprefix -I,$(INCS_PATHS))

NM_OBJS_PATH		= ./.objs/
NM_OBJS_NAME		= $(NM_SRCS:.c=.o)
NM_OBJS			= $(addprefix $(NM_OBJS_PATH), $(NM_OBJS_NAME))

OTOOL_OBJS_PATH		= ./.objs/
OTOOL_OBJS_NAME		= $(OTOOL_SRCS:.c=.o)
OTOOL_OBJS			= $(addprefix $(OTOOL_OBJS_PATH), $(OTOOL_OBJS_NAME))


DEPS			= $(NM_OBJS:.o=.d)
DEPS			+= $(OTOOL_OBJS:.o=.d)

LIB_PATHS		= ./libft/ ./printf/
LIBS			= $(addprefix -L,$(LIB_PATHS))

LDFLAGS			= $(LIBS) -lft -lftprintf



all: $(NAME) $(OTOOL)


$(NAME): $(NM_OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j8 -C $(PATHS);\
	)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OTOOL): $(OTOOL_OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j8 -C $(PATHS);\
	)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OTOOL_OBJS_PATH)%.o: $(OTOOL_SRCS_PATHS)%.c Makefile
	@mkdir -p $(OTOOL_OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

$(NM_OBJS_PATH)%.o: $(NM_SRCS_PATHS)%.c Makefile
	@mkdir -p $(NM_OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

clean:
	@$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) clean;)
	rm -rf $(NM_OBJS_PATH)
	rm -rf $(OTOOL_OBJS_PATH)

fclean:
	@$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) fclean;)
	rm -rf $(NM_OBJS_PATH)
	rm -rf $(OTOOL_OBJS_PATH)
	rm -f $(NAME)
	rm -f $(OTOOL)

re: fclean all

-include $(DEPS)
