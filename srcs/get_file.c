/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_file.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 16:24:04 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/11 17:23:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "errors.h"
#include "nm.h"
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

struct s_file	get_file(const char *file_name)
{
	int				fd;
	struct s_file	file;
	struct stat		f;

	ft_bzero(&file, sizeof(file));
	if ((fd = open(file_name, O_RDONLY)) == -1)
	{
		ft_perror(file_name, FILE_ERROR);
		return (file);
	}
	if (fstat(fd, &f) == -1 || !(f.st_mode & S_IFREG))
	{
		ft_perror(file_name, FILE_ERROR);
		close(fd);
		return (file);
	}
	file.file_content = mmap(NULL, f.st_size, PROT_READ | PROT_WRITE,
			MAP_PRIVATE, fd, 0);
	close(fd);
	if (file.file_content == NULL)
		ft_ferror("", MEM_ERROR);
	file.file_name = (char *)file_name;
	file.file_size = f.st_size;
	return (file);
}
