/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_load_sections.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 12:42:44 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 12:53:58 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "libft.h"
#include "errors.h"

static void		iter_load_sections(struct s_ofile *ofile)
{
	uint32_t				i;
	uint32_t				section_index;
	struct load_command		*lcs;
	struct mach_header		*head;

	lcs = ofile->lc;
	if (ofile->mh)
		head = ofile->mh;
	else
		head = (struct mach_header *)ofile->mh64;
	i = 0;
	section_index = 0;
	while (i < head->ncmds)
	{
		if (lcs->cmd == LC_SEGMENT || lcs->cmd == LC_SEGMENT_64)
			section_index = parse_segment(ofile, lcs, section_index);
		lcs = (struct load_command *)((char *)lcs + lcs->cmdsize);
		i++;
	}
}

void			parser_load_sections(struct s_ofile *ofile)
{
	if (ofile->mh)
	{
		ofile->sections = ft_memalloc(sizeof(struct section *) *
				(ofile->section_number + 1));
		if (ofile->sections == NULL)
			ft_ferror("", MEM_ERROR);
	}
	else
	{
		ofile->sections64 = ft_memalloc(sizeof(struct section *) *
				(ofile->section_number + 1));
		if (ofile->sections64 == NULL)
			ft_ferror("", MEM_ERROR);
	}
	iter_load_sections(ofile);
}
