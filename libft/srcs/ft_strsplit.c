/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:04:25 by mmeisson          #+#    #+#             */
/*   Updated: 2016/11/26 02:51:57 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_strlen_c(const char *s, char c)
{
	char		*tmp;

	tmp = (char*)s;
	while (*tmp && *tmp != c)
		tmp++;
	return (int)(tmp - s);
}

static int		ft_count_words(const char *s, char c)
{
	char		*tmp;
	int			ret;

	ret = 0;
	tmp = (char*)s;
	while (*tmp)
	{
		while (*tmp == c)
			tmp++;
		if (*tmp)
			ret++;
		while (*tmp && *tmp != c)
			tmp++;
	}
	return (ret);
}

char			**ft_strsplit(const char *s, char c)
{
	int		nb_words;
	char	**tab;
	char	*tmp;
	int		len;
	int		i;

	i = 0;
	tmp = (char*)s;
	while (*tmp == c)
		tmp++;
	nb_words = ft_count_words(tmp, c);
	if (!(tab = malloc(sizeof(char*) * (nb_words + 1))))
		return (NULL);
	while (*tmp)
	{
		len = ft_strlen_c(tmp, c);
		if (!(tab[i] = ft_strnew(len)))
			return (NULL);
		ft_strncpy(tab[i++], tmp, len);
		tmp += len;
		while (*tmp == c)
			tmp++;
	}
	tab[i] = NULL;
	return (tab);
}
