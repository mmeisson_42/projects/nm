/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avlnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/28 16:03:30 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/28 17:32:34 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_avl		*ft_avlnew(void *content, size_t content_size)
{
	t_avl		*new;

	if (!(new = malloc(sizeof(*new))))
		return (NULL);
	new->left = NULL;
	new->right = NULL;
	if (content != NULL && content_size != 0)
	{
		new->content = malloc(content_size);
		new->content_size = content_size;
		ft_memcpy(new->content, content, content_size);
	}
	else
	{
		new->content = NULL;
		new->content_size = 0;
	}
	return (new);
}
