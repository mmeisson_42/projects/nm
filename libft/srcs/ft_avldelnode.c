/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avldelnode.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/28 17:23:11 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/28 17:27:55 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_avldelnode(t_avl **node, void (*del)(void *, size_t))
{
	if (*node != NULL)
	{
		del((*node)->content, (*node)->content_size);
		free(*node);
		*node = NULL;
	}
}
