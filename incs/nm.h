/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 11:48:34 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/22 14:11:38 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NM_H
# define NM_H

# include <stdint.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <stdbool.h>

# include "libft.h"
# include "arch.h"

# define SWAP(var) swap_bytes(&var, sizeof(var))

struct			s_ofile
{
	char					*file;
	char					*arch_name;
	bool					reverse;

	struct mach_header		*mh;
	struct mach_header_64	*mh64;

	struct fat_arch			*arch;
	struct fat_arch_64		*arch64;

	struct load_command		*lc;

	uint32_t				section_number;

	struct section			**sections;
	struct section_64		**sections64;
	uint32_t				nsect_text;
	uint32_t				nsect_data;
	uint32_t				nsect_bss;

	struct symtab_command	*symtab;

	struct nlist			*nlist;
	struct nlist_64			*nlist64;

	char					*stringtable;
};

struct			s_file
{
	char	*file_name;
	char	*file_content;
	size_t	file_size;
};

typedef struct fat_arch_64	t_fa64;
typedef struct fat_arch		t_fa;

struct s_file	get_file(const char *file_name);
struct s_list	*handle_symtab(struct s_ofile *ofile);
char			get_type(struct s_ofile *ofile, struct nlist *sym);

void			swap_bytes(void *data, size_t mem_size);

void			print_symbols(struct s_ofile *ofile, struct s_list *symlist);

struct s_list	*get_ofiles(struct s_file *file);

t_fa			*curr_arch_in_fat_32(bool swap, struct fat_header *fh,
		struct fat_arch *fa);
t_fa64			*curr_arch_in_fat_64(bool swap, struct fat_header *fh,
		struct fat_arch_64 *fa);

int				arch_cmp(struct s_ofile *of1, struct s_ofile *of2);

/*
**	parse_something.c
*/
struct s_ofile	parse_header(struct s_file *file, char *str);
uint32_t		parse_segment(struct s_ofile *ofile, struct load_command *lc,
		uint32_t sect_index);
void			parser_loadfile(struct s_file *file, struct s_ofile *ofile,
		struct mach_header *head, struct load_command *lcs);
void			parser_symtab(struct s_file *file, struct s_ofile *ofile);
void			parser_sections(struct s_ofile *ofile);
void			parser_load_sections(struct s_ofile *ofile);

/*
**	free_ofile.c
*/
void			free_ofile(void *data, size_t size);

/*
**	for otool
*/
void			print_text_section(struct s_ofile *ofile);
void			print_ppc(struct s_ofile *ofile);

/*
**	safety
*/
void			check_fat(struct s_file *file);

/*
**	Shame
*/
void			*norme(struct s_list **lst, struct s_file *file);

#endif
