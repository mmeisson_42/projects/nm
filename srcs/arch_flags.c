/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arch_flags.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 12:58:52 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/11 11:51:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mach-o/arch.h>
#include <stdlib.h>
#include <stdbool.h>
#include "arch.h"
#include "nm.h"

static const struct s_arch_flag arch_flags[] = {
	{ "any", CPU_TYPE_ANY, CPU_SUBTYPE_MULTIPLE },
	{ "little", CPU_TYPE_ANY, CPU_SUBTYPE_LITTLE_ENDIAN },
	{ "big", CPU_TYPE_ANY, CPU_SUBTYPE_BIG_ENDIAN },
	{ "ppc64", CPU_TYPE_POWERPC64, CPU_SUBTYPE_POWERPC_ALL },
	{ "x86_64", CPU_TYPE_X86_64, CPU_SUBTYPE_X86_64_ALL },
	{ "ppc970-64", CPU_TYPE_POWERPC64, CPU_SUBTYPE_POWERPC_970 },
	{ "ppc", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_ALL },
	{ "i386", CPU_TYPE_I386, CPU_SUBTYPE_I386_ALL },
	{ "m68k", CPU_TYPE_MC680x0, CPU_SUBTYPE_MC680x0_ALL },
	{ "hppa", CPU_TYPE_HPPA, CPU_SUBTYPE_HPPA_ALL },
	{ "sparc", CPU_TYPE_SPARC, CPU_SUBTYPE_SPARC_ALL },
	{ "m88k", CPU_TYPE_MC88000, CPU_SUBTYPE_MC88000_ALL },
	{ "i860", CPU_TYPE_I860, CPU_SUBTYPE_I860_ALL },
	{ "arm", CPU_TYPE_ARM, CPU_SUBTYPE_ARM_ALL },
	{ "ppc601", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_601 },
	{ "ppc603", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_603 },
	{ "ppc603e", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_603e },
	{ "ppc603ev", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_603ev },
	{ "ppc604", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_604 },
	{ "ppc604e", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_604e },
	{ "ppc750", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_750 },
	{ "ppc7400", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_7400 },
	{ "ppc7450", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_7450 },
	{ "ppc970", CPU_TYPE_POWERPC, CPU_SUBTYPE_POWERPC_970 },
	{ "i486", CPU_TYPE_I386, CPU_SUBTYPE_486 },
	{ "i486SX", CPU_TYPE_I386, CPU_SUBTYPE_486SX },
	{ "pentium", CPU_TYPE_I386, CPU_SUBTYPE_PENT },
	{ "i586", CPU_TYPE_I386, CPU_SUBTYPE_586 },
	{ "pentpro", CPU_TYPE_I386, CPU_SUBTYPE_PENTPRO },
	{ "i686", CPU_TYPE_I386, CPU_SUBTYPE_PENTPRO },
	{ "pentIIm3", CPU_TYPE_I386, CPU_SUBTYPE_PENTII_M3 },
	{ "pentIIm5", CPU_TYPE_I386, CPU_SUBTYPE_PENTII_M5 },
	{ "pentium4", CPU_TYPE_I386, CPU_SUBTYPE_PENTIUM_4 },
	{ "m68030", CPU_TYPE_MC680x0, CPU_SUBTYPE_MC68030_ONLY },
	{ "m68040", CPU_TYPE_MC680x0, CPU_SUBTYPE_MC68040 },
	{ "hppa7100LC", CPU_TYPE_HPPA, CPU_SUBTYPE_HPPA_7100LC },
	{ "armv4t", CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V4T},
	{ "armv5", CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V5TEJ},
	{ "xscale", CPU_TYPE_ARM, CPU_SUBTYPE_ARM_XSCALE},
	{ "armv6", CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V6 },
	{ "armv7", CPU_TYPE_ARM, CPU_SUBTYPE_ARM_V7 },
	{ NULL, 0, 0 }
};

char	*get_arch_name(bool swap, cpu_type_t cputype, cpu_subtype_t cpusubtype)
{
	uint32_t		i;
	static char		unknow[] = "unknown";

	i = 0;
	if (swap)
	{
		SWAP(cputype);
		SWAP(cpusubtype);
	}
	while (arch_flags[i].name != NULL)
	{
		if (arch_flags[i].cputype == cputype &&
				(arch_flags[i].cpusubtype & ~CPU_SUBTYPE_MASK) ==
				(cpusubtype & ~CPU_SUBTYPE_MASK))
		{
			return (arch_flags[i].name);
		}
		i++;
	}
	return (unknow);
}
