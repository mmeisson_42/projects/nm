/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_header.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 11:17:21 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/22 14:12:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "errors.h"
#include "reverse.h"
#include <mach-o/loader.h>
#include <stdint.h>

static void			reverse_lcs_order(struct s_file *file,
		struct s_ofile *ofile, struct mach_header *head,
		struct load_command *lcs)
{
	uint32_t			i;

	i = 0;
	if (ofile->reverse == false)
		return ;
	while (i < head->ncmds)
	{
		if (file->file_size < ((uint64_t)lcs + 128)
				- (uint64_t)file->file_content)
			ft_ferror(file->file_name, CORRUPTED_ERROR);
		reverse_lcs_header(lcs);
		if (lcs->cmd == LC_SYMTAB)
			reverse_symtab(file, (struct symtab_command *)lcs);
		else if (lcs->cmd == LC_SEGMENT)
			reverse_segment_32((struct segment_command *)lcs);
		else if (lcs->cmd == LC_SEGMENT_64)
			reverse_segment_64((struct segment_command_64 *)lcs);
		lcs = (struct load_command *)((char *)lcs + lcs->cmdsize);
		i++;
	}
}

static uint32_t		set_mach_header(struct mach_header *head,
		struct s_ofile *ofile)
{
	uint32_t		head_size;

	if (head->magic == MH_MAGIC || head->magic == MH_CIGAM)
	{
		ofile->mh = (void *)head;
		head_size = sizeof(*ofile->mh);
		reverse_mach_header(ofile->mh, ofile->reverse);
		ofile->arch_name = get_arch_name(ofile->reverse, ofile->mh->cputype,
				ofile->mh->cpusubtype);
	}
	else
	{
		ofile->mh64 = (void *)head;
		head_size = sizeof(*ofile->mh64);
		reverse_mach_header_64(ofile->mh64, ofile->reverse);
		ofile->arch_name = get_arch_name(ofile->reverse, ofile->mh64->cputype,
				ofile->mh64->cpusubtype);
	}
	return (head_size);
}

struct s_ofile		parse_header(struct s_file *file, char *str)
{
	struct mach_header	*head;
	struct s_ofile		ofile;
	uint32_t			head_size;

	if (file->file_size < sizeof(struct mach_header_64))
		ft_ferror(file->file_name, CORRUPTED_ERROR);
	ft_bzero(&ofile, sizeof(ofile));
	ofile.file = str;
	head = (struct mach_header *)str;
	if (head->magic == MH_CIGAM || head->magic == MH_CIGAM_64)
		ofile.reverse = true;
	head_size = set_mach_header(head, &ofile);
	ofile.lc = (struct load_command *)(str + head_size);
	reverse_lcs_order(file, &ofile, head, ofile.lc);
	parser_loadfile(file, &ofile, head, ofile.lc);
	parser_load_sections(&ofile);
	parser_symtab(file, &ofile);
	return (ofile);
}
