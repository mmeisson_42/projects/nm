/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_ofile.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 11:01:57 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/13 11:03:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "libft.h"

void				free_ofile(void *data, size_t size)
{
	struct s_ofile	*ofile;

	if (data)
	{
		ofile = data;
		ft_memdel((void **)&ofile->sections);
		ft_memdel((void **)&ofile->sections64);
		free(data);
	}
	(void)size;
}
