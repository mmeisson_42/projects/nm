/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/14 23:00:52 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/21 13:36:56 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef REVERSE_H
# define REVERSE_H

# include <mach-o/loader.h>
# include <stdbool.h>

struct s_file;

void		reverse_symtab(struct s_file *file, struct symtab_command *sc);
void		reverse_segment_32(struct segment_command *sc32);
void		reverse_segment_64(struct segment_command_64 *sc64);
void		reverse_mach_header(struct mach_header *mh32, bool reverse);
void		reverse_mach_header_64(struct mach_header_64 *mh64, bool reverse);
void		reverse_lcs_header(struct load_command *lc);
void		reverse_nlist_32(struct nlist *nl, uint32_t nsym);
void		reverse_nlist_64(struct nlist_64 *nl, uint32_t nsym);

#endif
