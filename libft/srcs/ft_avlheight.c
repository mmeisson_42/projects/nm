/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_avlheight.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/28 16:11:06 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/28 16:29:10 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_avlheight(t_avl *node)
{
	size_t		h_left;
	size_t		h_right;

	if (node == NULL)
		return (0);
	h_left = ft_avlheight(node->left);
	h_right = ft_avlheight(node->right);
	return (h_left > h_right) ? (++h_left) : (++h_right);
}
