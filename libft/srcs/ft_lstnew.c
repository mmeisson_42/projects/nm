/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:59:12 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/13 10:21:44 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(const void *content, size_t content_size)
{
	t_list	*node;

	if (!(node = malloc(sizeof(*node))))
		return (NULL);
	if (content != NULL && content_size != 0)
	{
		if (!(node->content = ft_memdup(content, content_size)))
		{
			free(node);
			return (NULL);
		}
		node->content_size = content_size;
	}
	else
	{
		node->content = NULL;
		node->content_size = 0;
	}
	node->next = NULL;
	return (node);
}
