/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_bytes.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 17:34:19 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/13 17:34:31 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void		swap_bytes(void *mem, size_t mem_size)
{
	unsigned char	*mem_tmp;
	unsigned char	tmp;
	size_t			i;
	size_t			j;

	i = 0;
	j = mem_size - 1;
	mem_tmp = (unsigned char *)mem;
	while (i < j)
	{
		tmp = mem_tmp[i];
		mem_tmp[i] = mem_tmp[j];
		mem_tmp[j] = tmp;
		i++;
		j--;
	}
}
