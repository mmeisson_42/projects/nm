/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:57:17 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/20 19:05:59 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdint.h>

void	ft_bzero(void *s, size_t n)
{
	uint64_t	*big_part;
	uint8_t		*small_part;
	size_t		big;
	size_t		small;

	if (n >= sizeof(uint64_t))
	{
		big_part = (uint64_t *)s;
		big = n / sizeof(uint64_t);
		while (big--)
			*big_part++ = (uint64_t)0;
		small_part = (uint8_t *)big_part;
	}
	else
		small_part = (uint8_t *)s;
	small = n % sizeof(uint64_t);
	while (small--)
		*small_part++ = (uint8_t)0;
}
