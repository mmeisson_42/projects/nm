/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 13:41:13 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/13 13:20:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mach-o/loader.h>
#include <mach-o/nlist.h>
#include "nm.h"

static char	get_nsect(struct s_ofile *ofile, struct nlist *sym)
{
	if (sym->n_sect == ofile->nsect_text + 1)
		return ('t');
	else if (sym->n_sect == ofile->nsect_data + 1)
		return ('d');
	else if (sym->n_sect == ofile->nsect_bss + 1)
		return ('b');
	else
		return ('s');
}

static char	get_undf(struct nlist *sym)
{
	char	c;

	c = 'u';
	if (sym->n_value != 0)
		c = 'c';
	return (c);
}

/*
**	The '-' symbol shouldn't be displayed cause the opton to display
**	debug entries is not managed
*/

char		get_type(struct s_ofile *ofile, struct nlist *sym)
{
	uint8_t				c;

	if (sym->n_type & N_STAB)
		c = '-';
	else
	{
		c = sym->n_type & N_TYPE;
		if (c == N_UNDF)
			c = get_undf(sym);
		else if (c == N_PBUD)
			c = 'u';
		else if (c == N_ABS)
			c = 'a';
		else if (c == N_SECT)
		{
			c = get_nsect(ofile, sym);
		}
		else if (c == N_INDR)
			c = 'i';
		else
			c = '?';
		if (sym->n_type & N_EXT && c != '?')
			c -= 32;
	}
	return (c);
}
