/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   curr_arch_in_fat.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 11:59:52 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/11 11:56:01 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mach-o/fat.h>
#include "nm.h"

struct fat_arch		*curr_arch_in_fat_32(bool swap, struct fat_header *fh,
		struct fat_arch *fa)
{
	size_t		i;

	i = 0;
	while (i < fh->nfat_arch)
	{
		if (ft_strequ("x86_64", get_arch_name(swap, fa[i].cputype,
						fa[i].cpusubtype)))
			return (fa + i);
		i++;
	}
	return (NULL);
}

struct fat_arch_64	*curr_arch_in_fat_64(bool swap, struct fat_header *fh,
		struct fat_arch_64 *fa)
{
	size_t		i;

	i = 0;
	while (i < fh->nfat_arch)
	{
		if (ft_strequ("x86_64", get_arch_name(swap, fa[i].cputype,
						fa[i].cpusubtype)))
			return (fa + i);
		i++;
	}
	return (NULL);
}
