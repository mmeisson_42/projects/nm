/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_fat_size.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 10:04:28 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/21 13:03:33 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "errors.h"
#include <mach-o/fat.h>
#include <mach-o/loader.h>

void		check_fat32(struct s_file *file, struct fat_header *fat,
		uint32_t nfat_arch)
{
	struct fat_arch		*archs;
	uint32_t			offset;
	uint32_t			size;

	archs = (struct fat_arch *)(file->file_content + sizeof(struct fat_header));
	offset = archs[nfat_arch - 1].offset;
	size = archs[nfat_arch - 1].size;
	if (fat->magic == FAT_CIGAM)
	{
		SWAP(offset);
		SWAP(size);
	}
	if (file->file_size < offset + size)
		ft_ferror(file->file_name, CORRUPTED_ERROR);
}

void		check_fat64(struct s_file *file, struct fat_header *fat,
		uint32_t nfat_arch)
{
	struct fat_arch_64		*archs;
	uint64_t				offset;
	uint64_t				size;

	archs = (struct fat_arch_64 *)(file->file_content +
			sizeof(struct fat_header));
	offset = archs[nfat_arch - 1].offset;
	size = archs[nfat_arch - 1].size;
	if (fat->magic == FAT_CIGAM_64)
	{
		SWAP(offset);
		SWAP(size);
	}
	if (file->file_size < offset + size)
		ft_ferror(file->file_name, CORRUPTED_ERROR);
}

void		check_fat(struct s_file *file)
{
	struct fat_header	*fat;
	size_t				arch_size;
	uint32_t			nfat_arch;

	fat = (struct fat_header *)file->file_content;
	if (file->file_size < sizeof(*fat))
		ft_ferror(file->file_name, CORRUPTED_ERROR);
	if (fat->magic == FAT_MAGIC)
		arch_size = sizeof(struct fat_arch);
	else
		arch_size = sizeof(struct fat_arch_64);
	nfat_arch = fat->nfat_arch;
	if (fat->magic == FAT_CIGAM || fat->magic == FAT_CIGAM_64)
		SWAP(nfat_arch);
	if (file->file_size < sizeof(struct fat_header) + arch_size * nfat_arch)
		ft_ferror(file->file_name, CORRUPTED_ERROR);
	else if (fat->magic == FAT_MAGIC || fat->magic == FAT_CIGAM)
		check_fat32(file, fat, nfat_arch);
	else
		check_fat64(file, fat, nfat_arch);
}
