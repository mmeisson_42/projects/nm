/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse_2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 12:22:42 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/21 12:23:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "errors.h"

void	reverse_mach_header(struct mach_header *mh32, bool reverse)
{
	if (reverse == false)
		return ;
	SWAP(mh32->magic);
	SWAP(mh32->cputype);
	SWAP(mh32->cpusubtype);
	SWAP(mh32->filetype);
	SWAP(mh32->ncmds);
	SWAP(mh32->sizeofcmds);
	SWAP(mh32->flags);
}

void	reverse_mach_header_64(struct mach_header_64 *mh64, bool reverse)
{
	if (reverse == false)
		return ;
	SWAP(mh64->magic);
	SWAP(mh64->cputype);
	SWAP(mh64->cpusubtype);
	SWAP(mh64->filetype);
	SWAP(mh64->ncmds);
	SWAP(mh64->sizeofcmds);
	SWAP(mh64->flags);
}

void	reverse_symtab(struct s_file *file, struct symtab_command *sc)
{
	SWAP(sc->symoff);
	SWAP(sc->nsyms);
	SWAP(sc->stroff);
	SWAP(sc->strsize);
	if (file->file_size < sc->stroff + sc->strsize)
		ft_ferror(file->file_name, CORRUPTED_ERROR);
}

void	reverse_segment_32(struct segment_command *sc32)
{
	SWAP(sc32->vmaddr);
	SWAP(sc32->vmsize);
	SWAP(sc32->fileoff);
	SWAP(sc32->filesize);
	SWAP(sc32->maxprot);
	SWAP(sc32->initprot);
	SWAP(sc32->nsects);
	SWAP(sc32->flags);
}

void	reverse_segment_64(struct segment_command_64 *sc64)
{
	SWAP(sc64->vmaddr);
	SWAP(sc64->vmsize);
	SWAP(sc64->fileoff);
	SWAP(sc64->filesize);
	SWAP(sc64->maxprot);
	SWAP(sc64->initprot);
	SWAP(sc64->nsects);
	SWAP(sc64->flags);
}
