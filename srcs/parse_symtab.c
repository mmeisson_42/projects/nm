/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_symtab.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 12:45:10 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/22 14:21:47 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "libft.h"
#include "reverse.h"
#include "errors.h"

static void	check_nlist_32(struct s_file *file, struct nlist *nlist,
		uint32_t nsyms)
{
	uint32_t	i;

	i = 0;
	while (i < nsyms)
	{
		if (nlist[i].n_un.n_strx + 128 > file->file_size)
			ft_ferror(file->file_name, CORRUPTED_ERROR);
		i++;
	}
}

static void	check_nlist_64(struct s_file *file, struct nlist_64 *nlist,
		uint32_t nsyms)
{
	uint32_t	i;

	i = 0;
	while (i < nsyms)
	{
		if (nlist[i].n_un.n_strx + 128 > file->file_size)
			ft_ferror(file->file_name, CORRUPTED_ERROR);
		i++;
	}
}

void		parser_symtab(struct s_file *file, struct s_ofile *ofile)
{
	ofile->stringtable = ofile->file + ofile->symtab->stroff;
	if (ofile->mh)
	{
		if (file->file_size < ofile->symtab->symoff +
				ofile->symtab->nsyms * sizeof(struct nlist))
			ft_ferror(file->file_name, CORRUPTED_ERROR);
		ofile->nlist = (struct nlist *)(ofile->file + ofile->symtab->symoff);
		if (ofile->reverse)
			reverse_nlist_32(ofile->nlist, ofile->symtab->nsyms);
		check_nlist_32(file, ofile->nlist, ofile->symtab->nsyms);
	}
	else
	{
		if (file->file_size < ofile->symtab->symoff +
				ofile->symtab->nsyms * sizeof(struct nlist_64))
			ft_ferror(file->file_name, CORRUPTED_ERROR);
		ofile->nlist64 = (struct nlist_64 *)(ofile->file +
				ofile->symtab->symoff);
		if (ofile->reverse)
			reverse_nlist_64(ofile->nlist64, ofile->symtab->nsyms);
		check_nlist_64(file, ofile->nlist64, ofile->symtab->nsyms);
	}
}
