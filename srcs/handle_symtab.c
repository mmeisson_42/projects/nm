/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_symtab.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 11:43:59 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/13 13:47:36 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mach-o/loader.h>
#include <mach-o/nlist.h>
#include "libft.h"
#include "errors.h"
#include "nm.h"

static int		compare_sym(struct nlist *left, struct nlist *right,
		char *stringtable)
{
	int32_t	diff;

	diff = ft_strcmp(stringtable + left->n_un.n_strx,
			stringtable + right->n_un.n_strx);
	if (diff == 0)
		diff = (left->n_type & N_TYPE) - (right->n_type & N_TYPE);
	if (diff == 0)
		diff = left->n_value - right->n_value;
	return (diff);
}

static int		compare_sym_64(struct nlist_64 *left, struct nlist_64 *right,
		char *stringtable)
{
	int64_t		diff;

	diff = ft_strcmp(stringtable + left->n_un.n_strx,
			stringtable + right->n_un.n_strx);
	if (diff == 0)
		diff = (left->n_type & N_TYPE) - (right->n_type & N_TYPE);
	if (diff == 0)
		diff = left->n_value - right->n_value;
	return ((int)diff);
}

static t_list	*get_symbols_32(uint32_t nsyms, struct nlist *arr)
{
	uint32_t		i;
	struct s_list	*symlist;
	struct s_list	*sym;

	i = 0;
	symlist = NULL;
	while (i < nsyms)
	{
		sym = ft_lstnew_cpy(arr + i, sizeof(*arr));
		if (sym == NULL)
			ft_ferror("", MEM_ERROR);
		ft_lstadd(&symlist, sym);
		i++;
	}
	return (symlist);
}

static t_list	*get_symbols_64(uint32_t nsyms, struct nlist_64 *arr)
{
	uint32_t		i;
	struct s_list	*symlist;
	struct s_list	*sym;

	i = 0;
	symlist = NULL;
	while (i < nsyms)
	{
		if (!(arr[i].n_type & N_STAB))
		{
			sym = ft_lstnew_cpy(arr + i, sizeof(*arr));
			if (sym == NULL)
				ft_ferror("", MEM_ERROR);
			ft_lstadd(&symlist, sym);
		}
		i++;
	}
	return (symlist);
}

struct s_list	*handle_symtab(struct s_ofile *ofile)
{
	struct s_list	*symlist;

	if (ofile->mh)
	{
		symlist = get_symbols_32(ofile->symtab->nsyms, ofile->nlist);
		symlist = merge_list_r(symlist, compare_sym, ofile->stringtable);
	}
	else
	{
		symlist = get_symbols_64(ofile->symtab->nsyms, ofile->nlist64);
		symlist = merge_list_r(symlist, compare_sym_64, ofile->stringtable);
	}
	return (symlist);
}
