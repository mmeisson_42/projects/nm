/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/14 19:00:52 by mmeisson          #+#    #+#             */
/*   Updated: 2017/10/31 12:23:22 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

void	reverse_lcs_header(struct load_command *lc)
{
	SWAP(lc->cmd);
	SWAP(lc->cmdsize);
}

/*
**	nlist.n_type and nlist.n_sect are one byte long. No need to swap it
*/

void	reverse_nlist_32(struct nlist *nl, uint32_t nsyms)
{
	uint32_t		i;

	i = 0;
	while (i < nsyms)
	{
		SWAP(nl[i].n_un.n_strx);
		SWAP(nl[i].n_value);
		i++;
	}
}

void	reverse_nlist_64(struct nlist *nl, uint32_t nsyms)
{
	uint32_t		i;

	i = 0;
	while (i < nsyms)
	{
		SWAP(nl[i].n_un.n_strx);
		SWAP(nl[i].n_value);
		i++;
	}
}
