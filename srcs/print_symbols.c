/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_symbols.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/14 11:29:08 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/11 17:24:27 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"
#include "libft.h"
#include "nm.h"
#include <mach-o/loader.h>
#include <mach-o/nlist.h>

static void		print_symbol_64(struct s_ofile *ofile, struct nlist_64 *sym,
		char type)
{
	const char		spaces[] = "                ";

	if (!(sym->n_type & N_STAB))
	{
		if (((sym->n_type & N_TYPE) == N_UNDF ||
				(sym->n_type & N_TYPE) == N_INDR) && sym->n_value == 0)
		{
			ft_printf("%16s %c %s\n", spaces, type,
					ofile->stringtable + sym->n_un.n_strx);
		}
		else
		{
			ft_printf("%016llx %c %s\n", sym->n_value, type,
					ofile->stringtable + sym->n_un.n_strx);
		}
	}
}

static void		print_symbol(struct s_ofile *ofile, struct nlist *sym,
		char type)
{
	const char		spaces[] = "        ";

	if (!(sym->n_type & N_STAB))
	{
		if (((sym->n_type & N_TYPE) == N_UNDF ||
			(sym->n_type & N_TYPE) == N_INDR) && sym->n_value == 0)
		{
			ft_printf("%8s %c %s\n", spaces, type,
					ofile->stringtable + sym->n_un.n_strx);
		}
		else
		{
			ft_printf("%08llx %c %s\n", sym->n_value, type,
					ofile->stringtable + sym->n_un.n_strx);
		}
	}
}

void			print_symbols(struct s_ofile *ofile, struct s_list *symlist)
{
	char			type;
	struct nlist	*sym;

	while (symlist != NULL)
	{
		sym = symlist->content;
		if (*(ofile->stringtable + sym->n_un.n_strx))
		{
			type = get_type(ofile, symlist->content);
			if (ofile->mh)
				print_symbol(ofile, symlist->content, type);
			else
				print_symbol_64(ofile, symlist->content, type);
		}
		symlist = symlist->next;
	}
}
