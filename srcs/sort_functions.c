/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 12:14:44 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/11 12:21:59 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"

int		arch_cmp(struct s_ofile *of1, struct s_ofile *of2)
{
	uint32_t	cpu1;
	uint32_t	cpu2;

	if (of1->arch)
		cpu1 = of1->arch->cputype;
	else
		cpu1 = of1->arch64->cputype;
	if (of2->arch)
		cpu2 = of2->arch->cputype;
	else
		cpu2 = of2->arch64->cputype;
	return (cpu2 - cpu1);
}
