/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_sections.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 20:31:02 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/13 14:47:04 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "stdint.h"
#include "ft_stdio.h"

static void		print_text_32(struct s_ofile *ofile, struct section *sect)
{
	uint32_t		i;

	if (sect == NULL)
		return ;
	i = 0;
	if (ofile->reverse)
	{
		SWAP(sect->offset);
		SWAP(sect->size);
		SWAP(sect->addr);
	}
	while (i < sect->size)
	{
		if (i % 16 == 0)
		{
			ft_printf("%08llx\t", sect->addr);
			sect->addr += 16;
		}
		ft_printf("%.2hhx ", ofile->file[sect->offset + i]);
		i++;
		if (i % 16 == 0)
			ft_putchar('\n');
	}
	if (i % 16 != 0)
		ft_putchar('\n');
}

static void		print_text_64(struct s_ofile *ofile, struct section_64 *sect)
{
	uint64_t		i;

	if (sect == NULL)
		return ;
	i = 0;
	if (ofile->reverse)
	{
		SWAP(sect->offset);
		SWAP(sect->size);
		SWAP(sect->addr);
	}
	while (i < sect->size)
	{
		if (i % 16 == 0)
		{
			ft_printf("%016llx\t", sect->addr);
			sect->addr += 16;
		}
		ft_printf("%.2hhx ", ofile->file[sect->offset + i]);
		i++;
		if (i % 16 == 0)
			ft_putchar('\n');
	}
	if (i % 16 != 0)
		ft_putchar('\n');
}

void			print_text_section(struct s_ofile *ofile)
{
	if (ofile->arch && ft_strequ(ofile->arch_name, "ppc"))
		print_ppc(ofile);
	else if (ofile->mh)
		print_text_32(ofile, ofile->sections[ofile->nsect_text]);
	else
		print_text_64(ofile, ofile->sections64[ofile->nsect_text]);
}
