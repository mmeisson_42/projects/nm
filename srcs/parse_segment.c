/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_segment.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/14 13:00:43 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/13 13:19:32 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include <mach-o/loader.h>
#include <stdint.h>

static uint32_t		parse_segment_64(struct s_ofile *ofile,
		struct segment_command_64 *sc, uint32_t sect_index)
{
	uint32_t			i;
	struct section_64	*sect;

	i = 0;
	sect = (struct section_64 *)((char *)sc + sizeof(*sc));
	while (i < sc->nsects)
	{
		if (ft_strequ(sect[i].sectname, SECT_TEXT) &&
				ft_strequ(sect[i].segname, SEG_TEXT))
			ofile->nsect_text = sect_index;
		else if (ft_strequ(sect[i].sectname, SECT_DATA) &&
				ft_strequ(sect[i].segname, SEG_DATA))
			ofile->nsect_data = sect_index;
		else if (ft_strequ(sect[i].sectname, SECT_BSS) &&
				ft_strequ(sect[i].segname, SEG_DATA))
			ofile->nsect_bss = sect_index;
		ofile->sections64[sect_index++] = sect + i;
		i++;
	}
	return (sect_index);
}

static uint32_t		parse_segment_32(struct s_ofile *ofile,
		struct segment_command *sc, uint32_t sect_index)
{
	uint32_t		i;
	struct section	*sect;

	i = 0;
	sect = (struct section *)((char *)sc + sizeof(*sc));
	while (i < sc->nsects)
	{
		if (ft_strequ(sect[i].sectname, SECT_TEXT) &&
				ft_strequ(sect[i].segname, SEG_TEXT))
			ofile->nsect_text = sect_index;
		else if (ft_strequ(sect[i].sectname, SECT_DATA) &&
				ft_strequ(sect[i].segname, SEG_DATA))
			ofile->nsect_data = sect_index;
		else if (ft_strequ(sect[i].sectname, SECT_BSS) &&
				ft_strequ(sect[i].segname, SEG_DATA))
			ofile->nsect_bss = sect_index;
		ofile->sections[sect_index++] = sect + i;
		i++;
	}
	return (sect_index);
}

uint32_t			parse_segment(struct s_ofile *ofile,
		struct load_command *lc, uint32_t sect_index)
{
	if (ofile->mh)
		sect_index = parse_segment_32(ofile, (struct segment_command *)lc,
				sect_index);
	else
		sect_index = parse_segment_64(ofile, (struct segment_command_64 *)lc,
				sect_index);
	return (sect_index);
}
