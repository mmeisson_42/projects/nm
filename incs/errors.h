/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 12:16:07 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/14 10:28:36 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERRORS_H
# define ERRORS_H

# define FILE_ERROR			1
# define MEM_ERROR			2
# define OBJECT_ERROR		3
# define CORRUPTED_ERROR	4

# define TAB_LEN(tab) (sizeof(tab) / sizeof(tab[0]))

void		ft_perror(const char *msg, int errno);
void		ft_ferror(const char *msg, int errno);

#endif
