/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:00:07 by mmeisson          #+#    #+#             */
/*   Updated: 2015/11/27 15:40:53 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned char	*d;
	unsigned char	*s;

	if (!dest || !src)
		return (NULL);
	d = (unsigned char*)dest;
	s = (unsigned char*)src;
	if (d < s)
		ft_memcpy(dest, src, n);
	else if (s < d)
	{
		while (n--)
			d[n] = s[n];
	}
	return (dest);
}
