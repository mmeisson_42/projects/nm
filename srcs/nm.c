/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 11:47:56 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/16 11:08:00 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdint.h>
#include <mach-o/loader.h>
#include <mach-o/fat.h>
#include <stdbool.h>
#include <sys/mman.h>
#include "nm.h"
#include "arch.h"
#include "libft.h"
#include "errors.h"
#include "ft_stdio.h"

static void				process(struct s_ofile *ofile, int ac, char *file_name,
		bool several_arch)
{
	struct s_list	*symbols;

	symbols = handle_symtab(ofile);
	if (ofile->arch_name && several_arch)
		ft_printf("\n%s (for architecture %s):\n", file_name, ofile->arch_name);
	else if (ac != 2 || several_arch)
		ft_printf("\n%s:\n", file_name);
	print_symbols(ofile, symbols);
	ft_lstdel(&symbols, NULL);
}

int						nm(int ac, char *file_name)
{
	struct s_file	file;
	struct s_list	*ofiles;
	struct s_ofile	*ofile_current_arch;
	struct s_list	*beg_ofiles;

	file = get_file(file_name);
	if (file.file_content == NULL)
		return (-1);
	beg_ofiles = get_ofiles(&file);
	ofiles = beg_ofiles;
	ofile_current_arch = get_current_arch(ofiles);
	if (ft_lstlen(ofiles) == 1)
		process(ofiles->content, ac, file_name, false);
	else if (ofile_current_arch)
		process(ofile_current_arch, ac, file_name, false);
	else
		while (ofiles && ofiles->content)
		{
			process(ofiles->content, ac, file_name, true);
			ofiles = ofiles->next;
		}
	ft_lstdel(&beg_ofiles, free_ofile);
	munmap(file.file_name, file.file_size);
	return (0);
}

int						main(int ac, char *av[])
{
	int		i;

	i = 1;
	if (ac > 1)
	{
		while (i < ac)
		{
			nm(ac, av[i]);
			i++;
		}
	}
	else
		nm(ac, "./a.out");
	return (EXIT_SUCCESS);
}
