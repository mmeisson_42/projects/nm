/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/14 12:13:35 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/14 10:28:09 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "errors.h"
#include <stdlib.h>
#include <unistd.h>
#include "ft_stdio.h"

static char const	*g_errors[] = {
	[FILE_ERROR] = "An error happened with a file",
	[MEM_ERROR] = "Cannot allocate memory",
	[OBJECT_ERROR] = "This file was not recognized as a valid object file",
	[CORRUPTED_ERROR] = "This file seems corrupted",
};

void		ft_perror(const char *msg, int errno)
{
	const char	undefined[] = "Undefined error";
	const char	*error;

	if (errno >= 0 && errno < (int)TAB_LEN(g_errors) && g_errors[MEM_ERROR])
		error = g_errors[errno];
	else
		error = undefined;
	ft_dprintf(STDERR_FILENO, "%s %s: %d\n", msg, error, errno);
}

void		ft_ferror(const char *msg, int errno)
{
	ft_perror(msg, errno);
	exit(1);
}
