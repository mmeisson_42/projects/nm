/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arch.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/14 16:10:06 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/13 13:24:12 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARCH_H
# define ARCH_H

# include <mach/machine.h>
# include <stdbool.h>
# include "libft.h"

struct			s_arch_flag
{
	char			*name;
	cpu_type_t		cputype;
	cpu_subtype_t	cpusubtype;
};

char			*get_arch_name(bool swap, cpu_type_t cputype,
		cpu_subtype_t cpusubtype);
struct s_ofile	*get_current_arch(struct s_list *ofile);

#endif
