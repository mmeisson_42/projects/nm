/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_current_arch.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 20:23:59 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/11 21:04:04 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "nm.h"

struct s_ofile	*get_current_arch(struct s_list *ofiles)
{
	struct s_ofile *ofile;

	while (ofiles != NULL && ofiles->content != NULL)
	{
		ofile = ofiles->content;
		if (ft_strequ(ofile->arch_name, "x86_64"))
			return (ofile);
		ofiles = ofiles->next;
	}
	return (NULL);
}
