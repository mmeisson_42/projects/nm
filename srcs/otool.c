/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 19:56:35 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/16 11:08:16 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "arch.h"
#include "ft_stdio.h"
#include <sys/mman.h>
#include <stdbool.h>

void	process(struct s_ofile *this, char *file_name, bool several)
{
	if (several)
		ft_printf("%s (architecture %s):\n", file_name, this->arch_name);
	else
		ft_printf("%s:\n", file_name);
	ft_printf("Contents of (__TEXT,__text) section\n");
	print_text_section(this);
}

int		otool(char *file_name)
{
	struct s_file	file;
	struct s_list	*ofiles;
	struct s_list	*beg_ofiles;
	struct s_ofile	*ofile_current_arch;

	file = get_file(file_name);
	if (file.file_content == NULL)
		return (-1);
	if ((beg_ofiles = get_ofiles(&file)) == NULL)
		return (-1);
	ofiles = beg_ofiles;
	ofile_current_arch = get_current_arch(ofiles);
	if (ft_lstlen(ofiles) == 1)
		process(ofiles->content, file_name, false);
	else if (ofile_current_arch)
		process(ofile_current_arch, file_name, false);
	else
		while (ofiles && ofiles->content)
		{
			process(ofiles->content, file_name, true);
			ofiles = ofiles->next;
		}
	ft_lstdel(&beg_ofiles, free_ofile);
	munmap(file.file_content, file.file_size);
	return (0);
}

int		main(int ac, char **av)
{
	int		i;

	i = 1;
	if (ac > 1)
	{
		while (i < ac)
		{
			if (otool(av[i]) == -1)
				break ;
			i++;
		}
	}
	else
		otool("./a.out");
	return (EXIT_SUCCESS);
}
