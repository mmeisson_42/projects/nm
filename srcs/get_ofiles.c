/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_ofiles.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/15 10:59:57 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/21 13:43:26 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "libft.h"
#include "errors.h"
#include <mach-o/loader.h>
#include <stdint.h>
#include <mach-o/fat.h>

static void		extract_ofiles_32(struct s_list **ofiles, struct s_file *file,
		struct fat_arch *fa, bool swap)
{
	struct s_ofile		ofile;
	struct s_list		*node;

	if (swap)
		SWAP(fa->offset);
	ofile = parse_header(file, file->file_content + fa->offset);
	ofile.arch = fa;
	ofile.arch_name = get_arch_name(swap, fa->cputype, fa->cpusubtype);
	node = ft_lstnew(&ofile, sizeof(ofile));
	if (node == NULL)
		ft_ferror("", MEM_ERROR);
	ft_lstadd(ofiles, node);
}

static void		extract_ofiles_64(struct s_list **ofiles, struct s_file *file,
		struct fat_arch_64 *fa, bool swap)
{
	struct s_ofile		ofile;
	struct s_list		*node;

	if (swap)
		SWAP(fa->offset);
	ofile = parse_header(file, file->file_content + fa->offset);
	ofile.arch64 = fa;
	ofile.arch_name = get_arch_name(swap, fa->cputype, fa->cpusubtype);
	node = ft_lstnew(&ofile, sizeof(ofile));
	if (node == NULL)
		ft_ferror("", MEM_ERROR);
	ft_lstadd(ofiles, node);
}

static void		ofiles_32(struct s_list **ofiles, struct s_file *file,
		bool swap)
{
	struct fat_header	*fh;
	struct fat_arch		*fa;
	struct fat_arch		*curr_fa;
	uint32_t			i;

	i = 0;
	check_fat(file);
	fh = (struct fat_header *)file->file_content;
	fa = (struct fat_arch *)(file->file_content + sizeof(*fh));
	if (swap)
		swap_bytes(&fh->nfat_arch, sizeof(fh->nfat_arch));
	if ((curr_fa = curr_arch_in_fat_32(swap, fh, fa)))
		extract_ofiles_32(ofiles, file, curr_fa, swap);
	else
	{
		while (i < fh->nfat_arch)
		{
			extract_ofiles_32(ofiles, file, fa + i, swap);
			i++;
		}
	}
}

static void		ofiles_64(struct s_list **ofiles, struct s_file *file,
		bool swap)
{
	struct fat_header	*fh;
	struct fat_arch_64	*fa;
	struct fat_arch_64	*curr_fa;
	size_t				i;

	i = 0;
	check_fat(file);
	fh = (struct fat_header *)file->file_content;
	fa = (struct fat_arch_64 *)(file->file_content + sizeof(*fh));
	if (swap)
		swap_bytes(&fh->nfat_arch, sizeof(fh->nfat_arch));
	if ((curr_fa = curr_arch_in_fat_64(swap, fh, fa)))
		extract_ofiles_64(ofiles, file, curr_fa, swap);
	else
	{
		while (i < fh->nfat_arch)
		{
			extract_ofiles_64(ofiles, file, fa + i, swap);
			i++;
		}
	}
}

struct s_list	*get_ofiles(struct s_file *file)
{
	struct s_list	*lst;
	struct s_ofile	ofile;
	uint32_t		magic;

	lst = NULL;
	magic = *(uint32_t *)file->file_content;
	if (magic == MH_MAGIC || magic == MH_MAGIC_64 ||
			magic == MH_CIGAM || magic == MH_CIGAM_64)
	{
		ofile = parse_header(file, file->file_content);
		if (!(lst = ft_lstnew(&ofile, sizeof(ofile))))
			ft_ferror("", MEM_ERROR);
	}
	else if (magic == FAT_MAGIC || magic == FAT_CIGAM)
		ofiles_32(&lst, file, magic == FAT_CIGAM);
	else if (magic == FAT_MAGIC_64 || magic == FAT_CIGAM_64)
		ofiles_64(&lst, file, magic == FAT_CIGAM_64);
	else
		return (norme(&lst, file));
	lst = merge_list(lst, arch_cmp);
	if (magic == FAT_MAGIC_64 || magic == FAT_CIGAM_64 ||
			magic == FAT_CIGAM || magic == FAT_MAGIC)
		ft_lstback(&lst, ft_lstnew(NULL, 0));
	return (lst);
}
