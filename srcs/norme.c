/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   norme.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 13:40:19 by mmeisson          #+#    #+#             */
/*   Updated: 2017/11/21 13:41:36 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "nm.h"
#include "errors.h"
#include "libft.h"

void		*norme(struct s_list **lst, struct s_file *file)
{
	ft_lstdel(lst, NULL);
	ft_perror(file->file_name, OBJECT_ERROR);
	return (NULL);
}
